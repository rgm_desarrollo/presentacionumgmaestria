/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jroma
 */
public class JPAUtil {
    
    private static final String Persistencia="PERSISTENCE";
    private static EntityManagerFactory factory;
    
    public static EntityManagerFactory getEntityMaganerFacory()
    {
        if(factory==null)
        {
            factory = Persistence.createEntityManagerFactory(Persistencia);
        }
        return factory;
    }
    
    public static void Shutdown()
    {
        if(factory!=null)
        {
           factory.close();
        }
    }
    
}
