/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import controller.ClientesJpaController;
import controller.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import modelo.Clientes;
import modelo.JPAUtil;

/**
 *
 * @author jroma
 */
@ManagedBean(name = "DaoCliente")
@RequestScoped
public class DaoCliente {
    
        public String Nuevo()
        {
            return "";
        }
        
        
        public String guardar(Clientes cliente)
        {
             ClientesJpaController controller = new ClientesJpaController(JPAUtil.getEntityMaganerFacory());
             controller.create(cliente);            
            return "/Clientes/Listar.xhtml";
        }
        
        
        public List<Clientes> obtenerClientes()
        {
            ClientesJpaController controller = new ClientesJpaController(JPAUtil.getEntityMaganerFacory());
            return controller.findClientesEntities();           
        }
    
        
       public String Actualizar(Clientes cliente) throws Exception {
		
		  ClientesJpaController controller = new ClientesJpaController(JPAUtil.getEntityMaganerFacory());
                  controller.edit(cliente);
                  
                      FacesContext context = FacesContext.getCurrentInstance();
          context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Cliente Actualizado con Exito","Usuario eliminado exitosamente."));
         context.getExternalContext().getFlash().setKeepMessages(true);
                  
                  
		return  "/Clientes/Listar.xhtml";
	} 
    
    public String editar(Long id) {
		   
            ClientesJpaController controller = new ClientesJpaController(JPAUtil.getEntityMaganerFacory());

                    Clientes c = new Clientes();
                    c = controller.findClientes(id);                 

                    Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
                    sessionMap.put("cliente", c);
		return  "/Clientes/Modificar.xhtml";
                
	}
    
        
        public void Eliminar(Long id) throws IOException
        {
               ClientesJpaController controller = new ClientesJpaController(JPAUtil.getEntityMaganerFacory());
            try {
                controller.destroy(id);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(DaoCliente.class.getName()).log(Level.SEVERE, null, ex);
            }
               
            FacesContext.getCurrentInstance().getExternalContext().redirect("../faces/Clientes/Listar.xhtml");
            
            
            
            
        }
        
        
        
}
